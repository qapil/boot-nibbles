;같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같�
;컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�   
;  NIBBLES game. Created by Chaotik (c) 1998
;컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
;같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같�
            
SEG_A           SEGMENT
                ASSUME CS:SEG_A, DS:SEG_A    
                ORG     100H
START:                   
        mov ax, 00003h
        int 10h                 ; smaz obrazovku
                                
        call FRAME              ; oramuj obraz
        push ds
        pop es                  
                                
        xor ah, ah              
        int 16h                 ; cekej na klavesu
                                
        lea di, NIBBLE
        inc di
        inc di
        mov cx, 0000Ah
        mov ax, 007CCh
        rep stosw               ; zbytek NIBBLE
                          
        call NEWTARGET    
        call DRAWTARGET   
        call WRITELEN
MAIN:                          
        call REDRAW                              
        mov  cx, 00800h         ; zpomaleni
WAIT1:                                           
        mov  ah, 001h           ; test klavesnice
        int  16h             
        jz   LOOP1           
        xor  ah, ah          
        int  16h             
        cmp  ah, 001            ; stisknuto Esc
        je   QUIT            
        call SETDIREC           ; zmeni smer
LOOP1:                       
        loop WAIT1           
        mov  al, DIR1        
        mov  DIR2, al        
        call UPDATE          
        jc   QUIT               ; narazil na zed nebo se sezral
        jmp  MAIN            
QUIT:                        
                    
        cld         
        mov ax, 0B800h
        mov es, ax  
        lea si, TEXT3
        mov di, 00724h
        mov cx, 00009h
        mov ah, 08Ch
G1:                 
        lodsb          
        stosw       
        loop G1     
                      
        xor ah, ah              
        int 16h                 ; cekej na klavesu
        mov  ax, 00003h        
        int  10h                ; smaz obrazovku
                      
        ret
                             
;컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
;  procedures                
;컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
                             
FRAME PROC                   
        mov ax, 0B800h       
        mov es, ax           
                                  
        cld       
        xor di, di  
        mov ax, 003DBh
        mov cx, 00050h
        rep stosw               ; horni cara
                             
        mov bx, 000A0h          ; leva
        mov dx, 0013Eh          ; prava
        mov cx, 00017h  
F1:                     
        mov di, bx      
        stosw           
        add bx, 000A0h   
        mov di, dx    
        stosw         
        add dx, 000A0h        
        loop F1       
                      
        mov di, bx    
        mov cx, 00050h 
        rep stosw               ; dolni cara
                      
        lea si, TEXT1
        sub di, 0006Ch
        mov cx, 00018h
        mov ah, 031h    
F2:                    
        lodsb          
        stosw    
        loop F2
        
        mov di, 0003Eh
        mov cx, 00008h
F3:          
        lodsb
        stosw
        loop F3
                 
        ret      
FRAME ENDP       
                        
NEWTARGET PROC        
T0:                   
        mov ax, TARGET
T1:                   
        inc ax                     
        mov dx, 00065h 
        mul dx        
        dec ax        
                      
        mov cx, LEN
        mov si, NIBBLE
        push ax   
        call TESTUJ
        pop ax     
        jc T1      
        mov TARGET, ax
                   
        mov bx, ax  
        lea si, NIBBLE
        mov cx, LEN   
T2:                             ; kontroluj pozici cile
        lodsw         
        cmp ax, bx    
        je T0         
        loop T2       
                      
        ret           
NEWTARGET ENDP

TESTUJ          PROC            
; vstup:  ax - adresa noveho bodu
;         si - index zacatku pole na tesovani
;         cx - delka testovaneho pole
; vystup: OK - neni carry flag
;         KO - je carry flag
                      
        mov dx, ax            
        cmp ax, 000A0h                   
        jbe KO                  ; horni
        cmp ax, 00F00h                  
        jnb KO                  ; dolni
        mov bl, 0A0h
        div bl                         
        mov bl, ah                     
        cmp bl, 002h   
        jb  KO                  ; vlevo
        cmp bl, 09Eh
        jnb KO                  ; vpravo
                      
        cld           
TST:                  
        lodsw         
        cmp ax, dx    
        je KO         
        loop TST      
                                        
        clc           
        ret           
KO:                   
        stc           
        ret           
TESTUJ          ENDP  

DRAWTARGET      PROC            
        mov di, TARGET 
        mov ax, 0B800h
        mov es, ax    
        mov ax, 0C0Fh 
        stosw         
                      
        ret           
DRAWTARGET      ENDP  

REDRAW          PROC       
        mov ax, 0B800h
        mov es, ax              ; videoram
                       
        cld                     ; retezce odpredu
        lea si, LAST
        mov cx, 00002h   
NEXT:                 
        lodsw         
        mov di, ax    
                      
        mov ax, 00220h          ; vymaz konec
        cmp cl, 001h    
        jne WRITE                  
        mov al, 00Ah            ; zapis zacatek
WRITE:                            
        stosw        
        loop NEXT    
                     
        ret          
REDRAW          ENDP       
                     
SETDIREC        PROC 
        mov dl, ah   
        mov al, DIR2 
                     
        cmp dl, 04Bh            ; doleva
        jne @1               
        dec al               
        jmp @2                                
@1:                          
        cmp dl, 04Dh            ; doprava
        jne @3               
        inc al               
@2:                          
        and al, 3    
        mov DIR1, al 
@3:                  
        ret          
SETDIREC        ENDP 

UPDATE          PROC 
        push ds                                                  
        pop es                                     
        lea si, LAST
        mov di, si
        inc si
        inc si
        lodsw                   ; prvni NIBBLE
                                                   
        cmp DIR2, 0             ; nahoru
        jne U1                          
        sub ax, 000A0h                   
        jmp U4                          
U1:                                     
        cmp DIR2, 1             ; vpravo
        jne U2                          
        inc ax                          
        inc ax                          
        jmp U4                          
U2:                  
        cmp DIR2, 2             ; dolu
        jne U3                        
        add ax, 000A0h                 
        jmp U4                        
U3:                  
        dec ax                  ; vlevo
        dec ax        
U4:                  
        stosw        
                     
        mov cx, LEN  
        dec cx       
        call TESTUJ  
        jc RET2      
                     
        std                     ; zpracovat odzadu
        mov dx, si   
        mov di, dx   
        dec si       
        dec si       
        mov cx, LEN                               
        inc cx                                    
        rep movsw                                 
        cld                                       
                   
        mov si, dx          
        movsw                   ; zapis do LAST
                   
        mov si, di 
        lodsw                   ; prvni
        mov bx, TARGET
        cmp ax, bx              ; sezral cil ?
        jne RET1                ; ne          
                   
        call NEWTARGET
        mov di, si              ; prodluz
        mov cx, 0000Ah          ; pridavana delka
        add LEN, cx  
        rep stosw    

        call DRAWTARGET
        call WRITELEN
RET1:                              
        clc           
RET2:                 
        ret           
UPDATE          ENDP
                  
WRITELEN        PROC
        mov ax, 0B800h
        mov es, ax
        mov di, 00058h
        std                     ; odzadu
        mov ax, LEN  
        mov bx, 0000Ah
W1:                  
        xor dx, dx   
        div bx     
        push ax    
        mov al, dl  
        add al, 030h
        mov ah, 031h            ; barva
        stosw      
        pop ax     
        or ax, ax  
        jnz W1     
                   
        ret        
WRITELEN        ENDP   
                   
                      
;컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
;  datas            
;컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
                   
TARGET  DW 0064h   
                   
TEXT1   DB "Made by Chaotik (c) 1998"
TEXT2   DB "Length :"
TEXT3   DB "Game over"
                  
DIR1    DB 0                    ; smer ( 0 = ^, 1 = >, 2 = v, 3 = < )
DIR2    DB 0                    ; smer ( 0 = ^, 1 = >, 2 = v, 3 = < )
LEN     DW 0000Ah               ; delka
LAST    DW 007CCh               ; konec NIBBLE
NIBBLE  DW 0072Ch               ; souracnice NIBBLE (? >= 255)
                                  
SEG_A           ENDS    
                END START
        