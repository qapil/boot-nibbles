ASM  = tasm /w0 /m2
LINK = tlink /t

nibbles.com: nibbles.obj
	$(LINK) nibbles

nibbles.obj: nibbles.asm
	$(ASM) nibbles

clean:
	del nibbles.map
	del nibbles.obj
