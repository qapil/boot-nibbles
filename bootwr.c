#include <stdio.h>
#include <stdlib.h>

char buffer[512];

void main(int argc, char **argv)
{
  FILE *file;
  int i;

  if ( argc != 2 ) exit(1);

  asm {                                 // read bootsector
    mov al, 0x00
    mov cx, 0x01
    mov dx, 0x00
    lea bx, buffer
    int 0x25
  }

  file = fopen(argv[1], "rb+");
  fread(buffer, 11, 1, file);           // jmp + OEM ID
  fread(buffer+0x40, 53, 1, file);      // skip
  fread(buffer+0x40, 448, 1, file);     // code

  asm {                                 // write
    mov al, 0x00
    mov cx, 0x01
    mov dx, 0x00
    lea bx, buffer
    int 0x26
  }
  fclose(file);
}